/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.WebServiceRef;
import org.oorsprong.websamples.ArrayOftCountryCodeAndName;
import org.oorsprong.websamples.ArrayOftCurrency;
import org.oorsprong.websamples.CountryInfoService;

/**
 *
 * @author S_SAUKIN
 */
@WebServlet(urlPatterns = {"/testCurrency"})
public class testCurrency extends HttpServlet {

    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/webservices.oorsprong.org/websamples.countryinfo/CountryInfoService.wso.wsdl")
    public org.oorsprong.websamples.CountryInfoService service_1;

    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/webservices.oorsprong.org/websamples.countryinfo/CountryInfoService.wso.wsdl")
    public CountryInfoService service;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet testCurrency</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet testCurrency at " + request.getContextPath() + "</h1>");

            List<org.oorsprong.websamples.TCurrency> a = listOfCurrenciesByName().getTCurrency();

            for (org.oorsprong.websamples.TCurrency t : a) {
                out.println("<span> " + t.getSName() + " / " + t.getSISOCode() + "</span>");
            }

            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private ArrayOftCurrency listOfCurrenciesByName() {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        org.oorsprong.websamples.CountryInfoServiceSoapType port = service_1.getCountryInfoServiceSoap();
        return port.listOfCurrenciesByName();
    }



    

}
